/*
ALGORITMO ChecaImpar

Var

	num : real

Início

	Mostrar ("Digite um número qualquer:")

	Ler (num)

	Se (num % 2) != 0, faça

		Mostrar ("O número digitado é ÍMPAR!")

Fim
*/

import java.util.*;

public class ex02 {
	
	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);

		System.out.println("\n ==> PROGRAMA PARA VERIFICAR SE UM " +
                "NÚMERO QUALQUER É ÍMPAR <==\n");
		
		try {
			
			System.out.print(" Digite um número qualquer: ");
			
			float num = sc.nextInt();

			// Verifica se é ímpar

			if (num % 2 != 0) {
				
				System.out.println("\n O número digitado é ÍMPAR!");
				
			}
			
			
		} catch (Exception e) {
			
			System.out.println("\n Erro durante o processamento!");
			
		}
		
		sc.close();
		
	}
}
