/*
ALGORITMO FazerSoma

Var

	num1, num2, soma : inteiro

Início

    Mostrar ("Digite o primeiro número inteiro: ")

    Ler (num1)

    Mostrar ("Digite o segundo número inteiro: ")

    Ler (num2)

    soma <- num1 + num2

    Mostrar (soma)

Fim.
*/

import java.util.*;

public class ex01 {

	public static void main (String[] args) {
	
		Scanner sc = new Scanner(System.in);

        System.out.println("\n ==> PROGRAMA PARA EFETUAR A SOMA " +
                "DE DOIS INTEIROS <==\n");
			
		try {

			System.out.print(" Digite o primeiro número inteiro: ");

			int num1 = sc.nextInt();

			System.out.print("\n Digite o segundo número inteiro: ");

			int num2 = sc.nextInt();
			
			int summ = num1 + num2;

			System.out.println("\n A soma dos números digitados é: " + summ);
			
		} catch (Exception e) {
			
			System.out.println("\n Erro durante o processamento!");
			
		}
		
		sc.close();
		
	}

}