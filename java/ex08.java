import java.util.*;

public class ex08 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        try {

            // Reservatório 1

            System.out.print("\n Cota 1 (m): ");
            double quota1 = sc.nextDouble();

            System.out.print(" Diâmetro 1 (mm): ");
            double dm1 = sc.nextDouble();

            System.out.print(" Comprimento 1 (m): ");
            double cp1 = sc.nextDouble();

            // Reservatório 2

            System.out.print("\n Cota 2 (m): ");
            double quota2 = sc.nextDouble();

            System.out.print(" Diâmetro 2 (mm): ");
            double dm2 = sc.nextDouble();

            System.out.print(" Comprimento 2 (m): ");
            double cp2 = sc.nextDouble();

            // Reservatório 3

            System.out.print("\n Cota 3 (m): ");
            double quota3 = sc.nextDouble();

            System.out.print(" Diâmetro 3 (mm): ");
            double dm3 = sc.nextDouble();

            System.out.print(" Comprimento 3 (m): ");
            double cp3 = sc.nextDouble();

            System.out.print("\n Fator de Atrito: ");
            double fa = sc.nextDouble();

            double erro = 1;

            double bf = quota2;

            double q1 = Math.sqrt(((quota1 - bf) *
                    Math.pow( dm1 / 1000, 5)) /
                    (0.0827 * cp1 * fa));

            double q2 = Math.sqrt(((quota2 - bf) *
                    Math.pow(dm2 / 1000, 5)) /
                    (0.0827 * cp2 * fa));

            double q3 = Math.sqrt(((bf - quota3) *
                    Math.pow(dm3 / 1000, 5)) /
                    (0.0827 * cp3 * fa));

                if (q1 == q3) {

                    bf = quota2;

                }

                if (q1 > q3) {

                    while (erro > 0.000000001) {

                        bf = bf + 0.000001;

                        q1 = Math.sqrt(((quota1 - bf) *
                                Math.pow(dm1 / 1000, 5)) /
                                (0.0827 * cp1 * fa));

                        q2 = Math.sqrt(((bf - quota2) *
                                Math.pow(dm2 / 1000, 5)) /
                                (0.0827 * cp2 * fa));

                        q3 = Math.sqrt(((bf- quota3) *
                                Math.pow(dm3 / 1000, 5)) /
                                (0.0827 * cp3 * fa));

                        erro = (q1 - q2 - q3) / (q1);

                    }
                }

                if (q1 < q3) {

                    while (erro > 0.000000001) {

                        bf = bf - 0.0001;

                        q1 = Math.sqrt(((quota1 - bf) *
                                Math.pow(dm1 / 1000, 5)) /
                                (0.0827 * cp1 * fa));

                        q2 = Math.sqrt((Math.abs(quota2 - bf) *
                                Math.pow(dm2 / 1000, 5)) /
                                (0.0827 * cp2 * fa));

                        q3 = Math.sqrt(((bf - quota3) *
                                Math.pow(dm3 / 1000, 5))/
                                (0.0827 * cp3 * fa));

                        erro = (q3 - q1 - q2) / (q3);

                    }

                }

            System.out.print("\n Cota bifurcação: " + bf);
            System.out.print("\n Vazão trecho 1: " + q1);
            System.out.print("\n Vazão trecho 2: " + q2);
            System.out.print("\n Vazão trecho 3: " + q3);

        } catch (Exception e) {

            System.out.println("\n Erro durante o processameto!");

        }

    }

}