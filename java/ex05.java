/*
ALGORITMO TempMinMax

Var

    temp[7] : real
    i : inteiro
    v_min, v_max : real

Inicio

    Para i <- 1 até 7, faça

    Inicio

        Mostrar ("Digite a temperatura média: ")

        Ler (temp[i])

    Fim - Para

    vmax = temp[0]

    Para i <- 1 até 7, faça

    Inicio

        Se (temp[i] > v_max), faça
        
            v_max = temp[i];

    Fim - Para

    vmin = temp[0]

    Para i <- 1 até 7, faça

    Inicio

        Se (temp[i] > v_min), faça
        
            v_min = temp[i];

    Fim - Para

    Mostrar ("A temperatura mínima é", v_min)
    Mostrar ("A temperatura máxima é", v_max)

Fim
*/

import java.util.*;

public class ex05 {

    public static void main (String[] args) {

        System.out.println("\n ==> PROGRAMA PARA CALCUCAR " +
                "A TEMPERATURA MÍNIMA E MÁXIMA DA SEMANA <==\n");

        String[] dow = new String[] {"Domingo", "Segunda", "Terça",
                "Quarta", "Quinta", "Sexta", "Sábado"};

        float temp[] = new float[dow.length];

        Scanner sc = new Scanner(System.in);

        try {

            for (int i = 0; i < temp.length; i++) {

                System.out.print(" Digite a temperatura média em " +
                        "Celsius para " + dow[i] + ": ");

                temp[i] = sc.nextFloat();

            }

            // Verifica o valor máximo do array

            float v_max = temp[0];

            for (int i = 1; i < temp.length; i++) {

                if (temp[i] > v_max) {

                    v_max = temp[i];

                }
            }

            // Verifica o valor mínimo do array

            float v_min = temp[0];

            for (int i = 1; i < temp.length; i++) {

                if (temp[i] < v_min){

                    v_min = temp[i];

                }
            }

            System.out.println("\n A temperatura mínima " +
                    "da semana é: " + v_min);

            System.out.println("\n A temperatura máxima " +
                    "da semana é: " + v_max);

        } catch (Exception e) {

            System.out.println("\n Erro durante o processamento!");

        }

        sc.close();

    }
}
