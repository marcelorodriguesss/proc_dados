/*
Var

    vetor[6][2] : inteiro

    i, j : inteiro

    geo_ave : double

Inicio

    Para i <- 1 até 6, faça
    
    Inicio
    
        Para j <- 1 até 2, faça

        Inicio

        Mostrar ("Digite um valor inteiro: ")

        Ler (vetor[i][j])

        Fim - Para

    Fim - Para

    Para i <- 1 até 6, faça
    
    Inicio

        geo_ave = (array[i][0] * array[i][1]) ** 0.5

        Mostrar("A media geosmetria de ", array[i][0], " e ", 
                array[i][1], " é ", geo_ave )

    Fim - Para
*/

import java.util.*;

public class ex06 {

	public static void main (String[] args) {

        System.out.println("\n ==> PROGRAMA PARA CALCUCAR " +
                "A MÉDIA GEOMÉTRICA <==\n");
		
		int array[][] = new int[6][2];

        Scanner sc = new Scanner(System.in);
		
		try {
			
			for (int i = 0; i < array.length; i++) {

				for (int j = 0; j < array[0].length; j++) {
				
					System.out.print(" Digite um valor inteiro para a linha "
                            + (i + 1) + " coluna " + (j + 1) + ": ");

					array[i][j] = sc.nextInt();
					
				}
			}

            for (int i = 0; i < array.length; i++) {

                    double geo_ave = Math.pow((array[i][0] *
                            array[i][1]), 0.5);

			        System.out.println(" Média Geométrica de "
                            + array[i][0] + " e " + array[i][1] +
                            " é " + geo_ave);

			}

		} catch (Exception e) {
			
			System.out.println("Erro durante o processameto!");
			
		}
		
		sc.close();

	}

}
