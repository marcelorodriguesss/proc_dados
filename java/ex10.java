import java.util.*;

public class ex10 {
	 
	public static void main(String[] args) {
	
		String frase = new String ();

		StringBuffer fraseReversa = new StringBuffer();

		String fraseChar = new String();
		
		Scanner entrada = new Scanner (System.in);

		System.out.println("Digite uma frase qualquer:");
		
		try { 
			
			frase = entrada.nextLine();

			frase = frase.toUpperCase();

			fraseReversa.append(frase);

			fraseReversa = fraseReversa.reverse();
			
			fraseChar = fraseReversa.toString();
			
			char[] fraseArray = fraseChar.toCharArray();
			
			for (int i = 0; i < fraseArray.length; i++) {
				
				if(fraseArray[i]=='A' || fraseArray[i]=='E' || fraseArray[i]=='I'
                        || fraseArray[i]=='O' || fraseArray[i]=='U') {
				
					fraseArray[i]='*';
					
				}
				
			}
			
			System.out.println(fraseArray);
		
		} catch (Exception e) {
			
			System.out.println(" Erro durante o processamento!");
			
		}
		 
		entrada.close();
		
	}	

}
