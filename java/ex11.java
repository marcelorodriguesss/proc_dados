import java.io.*;
import java.util.*;

public class ex11 {

	public static void main(String[] args){
		
		boolean comp = true;

		String Resposta = null;

		int i = 0;

		Scanner sc = new Scanner(System.in);
				
		try {		
			
			File ddir = new File("HIDROLOGIA");

			if(! ddir.exists()) {

				ddir.mkdir();

			}
			
			FileWriter arquivo = new FileWriter("HIDROLOGIA/PRECIPITACAO.txt");
			
			PrintWriter escreve = new PrintWriter(arquivo);

			escreve.printf("ANO;PRECIPITAÇÃO ANUAL (mm)%n");

			try {

				while (comp) {
				
					System.out.println("Digite o ano: ");
					int ano = sc.nextInt();

					System.out.println("Digite a precipitação: ");
					float precipitacao = sc.nextFloat();

					escreve.printf(ano + ";" + precipitacao + "%n");
					System.out.println("Deseja continuar (S / N): ");
					Resposta = sc.next();

					i++;
										
					if (Resposta.equals("S") || Resposta.equals("s")) {
					
						comp = true;
						
					} else {
					
						comp = false;
					
					}
				
				}
				
				System.out.println("Este arquivo possui " + i + " anos de dados.");
				
			} catch (Exception e) {
				
				System.out.println("Ocorreu um erro durante a leitura!");
				
			}
				
			escreve.close();
			sc.close();
			
			System.out.println("");
			System.out.println("---------------------------------------------");
			System.out.println("");
			System.out.println("Lendo o arquivo gerado...");
			System.out.println("");
			System.out.println("---------------------------------------------");
			System.out.println("");
			
			FileReader ler = new FileReader("C:\\HIDROLOGIA\\precipitacao.txt");
			BufferedReader lerArq = new BufferedReader(ler);
			
			String Linha = lerArq.readLine();
			
			while (Linha != null) {
								
				System.out.println(Linha);
				Linha = lerArq.readLine();
				
			}
						
			ler.close();
			
		} catch (Exception e) {
			
			System.out.println("Ocorreu um erro durante a leitura!");
			
		}
		
	}
	
}
