import java.io.*;

public class ex12 {

    public static void main (String[] args) throws IOException {

        float area_m2 = 35000000;

        double q[] = new double[]{5, 5, 30, 50, 47, 35, 21, 13, 9, 7, 5};
        double qb[] = new double[q.length];
        double qs[] = new double[q.length];
        double qu[] = new double[q.length];
        double hu_cm[] = new double[q.length];  // hidrograma unitário

        // Determinar o HU de uma precipitação isolada de 1h (Tabela 1)

        // calcula vazão de base

        // solução analítica para encontrar ta e ti

        boolean flag = false;

        qb[0] = q[0];
        qs[0] = q[0] - qb[0];

        for (int i = 1; i < q.length; i++) {
            if (flag) {
                qb[i] = 5 + ((4 / 3.0) * (i + 1 - 2));
                qs[i] = q[i] - qb[i];

                if (qs[i] <= 0.0) {
                    qs[i] = 0;
                    qb[i] = q[i];
                    flag = false;
                }
            } else {
                if (q[i] > q[i - 1]) {
                    qb[i] = 5 + ((4 / 3.0) * (i + 1 - 2));
                    qs[i] = q[i] - qb[i];
                    flag = true;
                    if (qs[i] <= 0.0) {
                        qs[i] = 0;
                        qb[i] = q[i];
                        flag = false;
                    }
                } else {
                    qb[i] = q[i];
                    qs[i] = q[i] - qb[i];
                }
            }
        }

        double sum_qs = 0;

        for (int i = 0; i < q.length; i++) {
            sum_qs += qs[i];
        }

        double vol_s_m3 = sum_qs * 3600.0;

        // double vol_t_m3 = 24 * td * area_m2 * 1000000;

        double pef_mm = (vol_s_m3 / area_m2) * 1000.;

        // double ief_mm_h = pef_mm / td;

        // double c = vol_s_m3 / vol_t_m3 ;

        double pef_cm = pef_mm / 10;

        for (int i = 0; i < q.length; i++) {
            qu[i] = (qs[i] / pef_cm);
        }

        for (int i = 0; i < q.length; i++) {
            hu_cm[i] = ((qu[i] / area_m2) * 3600) * 10;
        }

        FileWriter fr1 = new FileWriter("hidrograma.txt");
        PrintWriter out1 = new PrintWriter(fr1);
        for(int i = 0; i < hu_cm.length; i++){
            out1.write(Double.toString(hu_cm[i]));
            out1.write("\n");
        }
        out1.flush();
        out1.close();

        // Determinar o escoamento resultante para outro evento
        // com precipitações apresentadas na Tabela 02.

        // double qu[] = new double[]{0, 12.1, 27.3, 24.2, 18.2, 10.9, 4.5, 0};  // teste

        double p1_cm = 3.0;  // 30 mm

        double p2_cm = 2.0;  // 20 mm

        double escoa_sup_p1[] = new double[qu.length];

        double escoa_sup_p2[] = new double[qu.length];

        for (int i = 0; i < qu.length; i++) {
            escoa_sup_p1[i] = p1_cm * qu[i];
            escoa_sup_p2[i] = p2_cm * qu[i];
        }

        double escoa_sup_total[] = new double[qu.length + 1];

        escoa_sup_total[0] = escoa_sup_p1[0];

        escoa_sup_total[escoa_sup_total.length - 2] = escoa_sup_p2[escoa_sup_p2.length - 2];

        for (int i = 1; i < qu.length - 1; i++) {
            escoa_sup_total[i] = escoa_sup_p1[i] + escoa_sup_p2[i - 1];
        }

        FileWriter fr2 = new FileWriter("hietograma.txt");
        PrintWriter out2 = new PrintWriter(fr2);
        for(int i = 0; i < escoa_sup_total.length; i++){
            out2.write(Double.toString(escoa_sup_total[i]));
            out2.write("\n");
        }
        out2.flush();
        out2.close();

    }

}