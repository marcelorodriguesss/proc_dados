/*
ALGORITMO LadosTriangulo

Var

	lado_a, lado_b, lado_c: inteiro

Início

	Mostrar ("Digite o valor do primeiro lado do triângulo:")

	Ler (lado_a)

	Mostrar ("Digite o valor do segundo lado do triângulo:")

	Ler (lado_b)

	Mostrar ("Digite o valor do terceiro lado do triângulo:")

	Ler (lado_c)

	Se (lado_a <= 0 || lado_b <= 0 || lado_c <= 0), faça

	    Mostrar ("Triângulo inválido!")

	senão, Se (lado_a > lado_b + lado_c || lado_b > lado_a + lado_c || lado_c > lado_a + lado_b), faça

        Mostrar ("Triângulo inválido!")

    senão, faça

        Se (lado_a == lado_b && lado_b == lado_c), faça

            Mostrar ("Triângulo EQUILÁTERO!")

        senão, se (lado_b == lado_c || lado_a == lado_b || lado_c == lado_a), faça

            Mostrar ("Triângulo ISÓSCELES!")

		senão, faça

			Mostrar ("Triângulo ESCALENO!")

Fim
*/

import java.util.*;

public class ex03 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

        System.out.println("\n ==> PROGRAMA PARA VERIFICAR OS LADOS " +
                "DE UM TRIÂNGULO <==\n");

		try {
		
			System.out.print(" Digite o valor do primeiro " +
                    "lado do triângulo: ");

			int aa = sc.nextInt();

			System.out.print("\n Digite o valor do segundo " +
                    "lado do triângulo: ");

			int bb = sc.nextInt();

			System.out.print("\n Digite o valor do terceiro " +
                    "lado do triângulo: ");

			int cc = sc.nextInt();
			
			if (aa <= 0 || bb <= 0 || cc <= 0) {

                System.out.println("\n Triângulo inválido! ");

            } else if (aa > bb + cc || bb > aa + cc || cc > aa + bb) {

                System.out.println("\n Triângulo inválido! ");

            } else {

			    if (aa == bb && bb == cc) {

                    System.out.print("\n Triângulo EQUILÁTERO! ");

                } else if (bb == cc || aa == bb || cc == aa) {

                    System.out.println("\n Triângulo ISÓSCELES! ");

                } else {

                    System.out.println("\n Triângulo ESCALENO! ");

                }
            }
			
		} catch (Exception e) {
			
			System.out.println("\n Erro durante o processamento!");
			
		}
		
		sc.close();
	}

}
