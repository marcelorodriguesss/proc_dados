import java.util.*;

public class ex07 {

    public static void main (String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("\n ++++++++++++ MENU ++++++++++++");

        System.out.print("\n (1) Perda de carga");

        System.out.print("\n (2) Diâmentro");

        System.out.println("\n (3) Vazão");

        try {

            System.out.print("\n Digite uma das opções acima: ");

            int num1 = sc.nextInt();

            if (num1 == 1) {

                // Valores de entrada

                System.out.println("\n   ==> Opção 1: Perda de carga");

                System.out.println("\n   ==> Entre com os parâmetros abaixo");

                System.out.print("\n   ==> Viscosidade Cinemática (kg/s.m): ");
                double vc = sc.nextDouble();

                System.out.print("   ==> Massa específica (kg/m3): ");
                double me = sc.nextDouble();

                System.out.print("   ==> Rugosidade (mm): ");
                double rg = sc.nextDouble();

                System.out.print("   ==> Comprimento (m): ");
                double cp = sc.nextDouble();

                System.out.print("   ==> Diâmetro (mm): ");
                double dm = sc.nextDouble();

                System.out.print("   ==> Vazão (m3/s): ");
                double q = sc.nextDouble();

                // cálculo

                double reynolds = (me * 4 * q) /
                        (Math.PI * (dm / 1000) * vc);

                double fa = (0.25) / Math.pow((Math.log10((rg /
                        (3.7 * dm)) + (5.74 / (Math.pow(reynolds, 0.9))))), 2);

                double pc = 0.0827 * (cp * fa *
                        (Math.pow(q, 2) / Math.pow((dm / 1000), 5)));

                System.out.println("   ==> Valor perda de carga: " + pc);

            } else if (num1 == 2) {

                // Valores de entrada

                System.out.println("\n   ==> Opção 2: Diâmentro");

                System.out.println("\n   ==> Entre com os parâmetros abaixo");

                System.out.print("\n   ==> Viscosidade Cinemática (kg/s.m): ");
                double vc = sc.nextDouble();

                System.out.print("   ==> Massa específica (kg/m3): ");
                double me = sc.nextDouble();

                System.out.print("   ==> Rugosidade (mm): ");
                double rg = sc.nextDouble();

                System.out.print("   ==> Comprimento (m): ");
                double cp = sc.nextDouble();

                System.out.print("   ==> Vazão (m3/s): ");
                double q = sc.nextDouble();

                System.out.print("   ==> Perdade de carga (m): ");
                double pc = sc.nextDouble();

                // cálculo

                double fa = 0.02;

                double erro = 1;

                double dm = (Math.pow(((0.0827 * cp * fa *
                        Math.pow(q, 2)) / pc), (1.0 / 5))) * 1000;

                while (erro > 0.002) {

                    double reynolds = (me * 4 * q) /
                            (Math.PI * (dm / 1000) * vc);

                    double fa_up = (0.25) /
                            Math.pow((Math.log10((rg / (3.7 * dm)) +
                                    (5.74 / (Math.pow(reynolds, 0.9))))), 2);

                    erro = Math.abs(fa_up - fa) /
                            fa_up;

                    fa = fa_up;

                    dm = (Math.pow(((0.0827 * cp *
                            fa * Math.pow(q, 2)) /
                            pc), (1.0 / 5))) * 1000;

                }

                System.out.println("   ==> Valor diâmetro: " + dm);

            } else if (num1 == 3) {

                // Valores de entrada

                System.out.println("\n   ==> Opção 2: Vazão");

                System.out.println("\n   ==> Entre com os parâmetros abaixo");

                System.out.print("\n   ==> Viscosidade Cinemática (kg/s.m): ");
                double vc = sc.nextDouble();

                System.out.print("   ==> Massa específica (kg/m3): ");
                double me = sc.nextDouble();

                System.out.print("   ==> Rugosidade (mm): ");
                double rg = sc.nextDouble();

                System.out.print("   ==> Comprimento (m): ");
                double cp = sc.nextDouble();

                System.out.print("   ==> Perdade de carga (m): ");
                double pc = sc.nextDouble();

                System.out.print("   ==> Diâmetro (mm): ");
                double dm = sc.nextDouble();

                // cálculo

                double fa = 0.02;

                double erro = 1;

                double q = Math.sqrt((pc *
                        Math.pow((dm / 1000), 5)) /
                        (0.0827 * cp * fa));

                while (erro > 0.002) {

                    double reynolds = (me * 4 * q) /
                            (Math.PI * (dm / 1000) * vc);

                    double fa_up = (0.25) /
                            Math.pow((Math.log10((rg / (3.7 * dm))
                                    + (5.74 / (Math.pow(reynolds, 0.9))))), 2);

                    erro = Math.abs(fa_up - fa) /
                            fa_up;

                    fa = fa_up;

                    q = Math.sqrt((pc * Math.pow((dm / 1000), 5)) /
                            (0.0827 * cp * fa));

                }

                System.out.println("   ==> Valor vazão: " + q);

            } else {

                System.out.println("\n ==> Escolha uma das opções válidas!");

            }

        } catch (Exception e) {

            System.out.println("\n Erro durante o processameto!");

        }

        sc.close();

    }

}
