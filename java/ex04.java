/*
ALGORITMO MediaArit

Var

    vetor[10]: float

    i, ave : float

Início

    ave <- 0

    i <- 0

    Para i <- 1 até 10, faça

    Início

        Mostrar ("Digite o valor para o espaço" + (i + 1) + "do array")

        Ler (Vetor[i])

        ave <- ave + vetor[i] / 10.0

    Fim - Para

    Mostrar ("A média é:: ", ave)

Fim
*/

import java.util.*;

public class ex04 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("\n ==> PROGRAMA PARA CALCUCAR A " +
                "MÉDIA ARITMÉTICA ENTRE DEZ NÚMEROS <==\n");

        float ave = 0;

        float array[] = new float[10];

        try {

            for (int i = 0; i < array.length; i++) {

                System.out.print(" Digite o valor para o espaço "
                        + (i + 1) + " do array: ");

                array[i]= sc.nextFloat();

                ave += array[i] / array.length;

            }

            System.out.println("\n A média é: " + ave );


        } catch (Exception e) {

            System.out.println("\n Erro durante o processamento!");

        }

        sc.close();

    }

}
