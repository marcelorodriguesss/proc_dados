import java.util.*;

public class ex09 {

	public static void main (String[] args) {

		int op;
			
		Scanner sc = new Scanner(System.in);

		try {
				
			System.out.println("\n === MENU ===");

			System.out.println("\n 1 Adição.");

			System.out.println(" 2 Subtração.");

			System.out.println(" 3 Multiplicação.");

			System.out.println(" 4 Divisão.");

			System.out.print("\n Digite uma das opções acima: ");

			op = sc.nextInt();
			
			switch (op) {
			
				case 1: cal_add(); break;

				case 2: cal_sub(); break;

				case 3: calc_mult(); break;

				case 4: calc_div(); break;
			
			}
			
		} catch(Exception e) {
				
			System.out.println(" Erro durante o processamento!");
				
		}
		
		sc.close();
		
	}
	
	static void cal_add() {
		
		float num1;

		float num2;

		float ssum;

		Scanner scssum = new Scanner(System.in);

		try {

			System.out.print(" Digite o primeiro número: ");

			num1 = scssum.nextInt();

			System.out.print(" Digite o segundo número: ");

			num2 = scssum.nextInt();
			
			ssum = num1 + num2;
			
			System.out.println(" A soma é: " + ssum);
			
		} catch (Exception e) {
			
			System.out.println(" Erro durante o processamento!");
			
		}

		scssum.close();
		
	}
	
	static void cal_sub() {
		
		float num1;

		float num2;

		float ssub;

		Scanner scssub = new Scanner(System.in);

		try {

			System.out.print(" Digite o primeiro número: ");

			num1 = scssub.nextInt();

			System.out.print(" Digite o segundo número: ");

			num2 = scssub.nextInt();
			
			ssub = num1 - num2;
			
			System.out.println(" A Subtração é: " + ssub);
			
		} catch (Exception e) {
			
			System.out.println(" Erro durante o processamento!");
			
		}
		
		scssub.close();
				
	}
	
	static void calc_mult() {
		
		float num1;

		float num2;

		float mmult;

		Scanner scmmult = new Scanner(System.in);

		try {

			System.out.print(" Digite o primeiro número: ");

			num1 = scmmult.nextInt();

			System.out.print(" Digite o segundo número: ");

			num2 = scmmult.nextInt();
			
			mmult = num1 * num2;
			
			System.out.println(" A multiplicação é: " + mmult);
			
		} catch (Exception e) {
			
			System.out.println(" Erro durante o processamento!");
			
		}
		
		scmmult.close();
		
	}
	
	static void calc_div() {
		
		float num1;

		float num2;

		float ddiv;

		Scanner scddiv = new Scanner(System.in);

		try {

			System.out.print(" Digite o primeiro número: ");

			num1 = scddiv.nextInt();

			System.out.print(" Digite o segundo número: ");

			num2 = scddiv.nextInt();
			
			ddiv = num1 / num2;
			
			System.out.println(" A divisão é: " + ddiv);
			
		} catch (Exception e) {
			
			System.out.println(" Erro durante o processamento!");
			
		}
		
		scddiv.close();
	}
			
}
